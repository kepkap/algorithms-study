package my.study.algorythms.util;

import java.util.Iterator;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public class Stringify {

    public static <T> String asString(Iterable<T> s) {
        StringBuilder res = new StringBuilder(" ");
        Iterator<T> iterator = s.iterator();
        while (iterator.hasNext()) {
            res.append(iterator.next());
            if (iterator.hasNext()) {
                res.append(",");
            }
        }
        res.append(" ");
        return res.toString();
    }
}
