package my.study.algorythms.stack;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public interface Stack<T> extends Iterable<T> {

    T push(T item);

    T pop();

    T peek();

    boolean isEmpty();

    int size();
}
