package my.study.algorythms.stack;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public class LinkedStack<T> implements Stack<T> {

    private Node first;
    private int N;


    @Override
    public T push(T item) {
        first = new Node(item, first);
        N++;
        return item;
    }

    @Override
    public T pop() {
        T item = first.item;
        first = first.next;
        N--;
        return item;
    }

    @Override
    public T peek() {
        return first.item;
    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public int size() {
        return N;
    }

    private class Node {
        T item;
        Node next;

        Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            if (item != null ? !item.equals(node.item) : node.item != null) return false;
            if (next != null ? !next.equals(node.next) : node.next != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = item != null ? item.hashCode() : 0;
            result = 31 * result + (next != null ? next.hashCode() : 0);
            return result;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedIterator();
    }

    private class LinkedIterator implements Iterator<T> {

        Node current = first;

        Node remFirst = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            if (!remFirst.equals(first)) {
                throw new ConcurrentModificationException();
            }
            T item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() {

        }
    }
}
