package my.study.algorythms.bag;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public class LinkedBag<T> implements Bag<T> {

    private Node first;
    private int N;

    private class Node {
        T item;
        Node next;

        Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }


    @Override
    public void add(T item) {
        first = new Node(item, first);
        N++;
    }

    public T remove(int index) {
        Node previous = first;

        Node current = previous;

        if (index == 0) {
            T item = first.item;
            first = first.next;
            return item;
        }

        for (int i = 0; i < size(); i++) {
            if (i == index) {
                T item = current.item;
                previous.next = current.next;
                return item;
            }
            previous = current;
            current = current.next;
        }
        throw new IllegalArgumentException("out of bounds");
    }

    public T removeAfter(T item) {
        Node current = first;
        while (current != null) {

            if (current.item.equals(item)) {
                T el = current.next.item;
                current.next = current.next.next;
                return el;
            }
            current = current.next;
        }
        return null;
    }

    public T insertAfter(T subjectItem, T newItem) {
        Node current = first;
        while (current != null) {
            if (current.item.equals(subjectItem)) {
                Node in = new Node(newItem, current.next);

                current.next = in;
                return newItem;
            }
            current = current.next;
        }
        return newItem;
    }

    @Override
    public void reverse() {
        first = reverse(first);
    }

    private Node reverse(Node first) {

        if (first == null) {
            return null;
        }
        if (first.next == null) {
            return first;
        }
        Node second = first.next;
        Node rest = reverse(second);
        second.next = first;
        first.next = null;
        return rest;

    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public int size() {
        return N;
    }

    @Override
    public Iterator<T> iterator() {
        return new NormalIterator();
    }

    private class NormalIterator implements Iterator<T> {
        Node current = first;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            T item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() {

        }
    }

    private class RandomIterator implements Iterator<T> {
        Node current = first;

        Random random = new Random();



        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            random.nextInt(N);


            T item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() {

        }
    }
}
