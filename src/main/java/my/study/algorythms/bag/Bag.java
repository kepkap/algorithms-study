package my.study.algorythms.bag;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public interface Bag<T> extends Iterable<T> {

    void add(T item);

    boolean isEmpty();

    int size();

    T remove(int index);

    T removeAfter(T item);

    T insertAfter(T subjectItem, T newItem);

    void reverse();
}
