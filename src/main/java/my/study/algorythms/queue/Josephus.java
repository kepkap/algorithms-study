package my.study.algorythms.queue;

import my.study.algorythms.util.StdOut;
import my.study.algorythms.util.Stringify;

import static my.study.algorythms.util.StdOut.print;
import static my.study.algorythms.util.StdOut.println;

/**
 * Created by dkuchugurov on 29.12.2014.
 */
public class Josephus {
    public static void main(String[] args) {
        int M = 2;//Integer.parseInt(args[0]);
        int N = 7;//Integer.parseInt(args[1]);

        // initialize the queue
        Queue<Integer> q = new LinkedQueue<Integer>();
        for (int i = 0; i < N; i++) {
            q.enqueue(i);
        }

        while (!q.isEmpty()) {
            println(Stringify.asString(q));
            for (int i = 0; i < M - 1; i++) {
                q.enqueue(q.dequeue());
                println(Stringify.asString(q));
            }
            println(q.dequeue() + " ");
        }
        println();
    }
}