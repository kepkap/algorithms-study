package my.study.algorythms.queue;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public interface Queue<T> extends Iterable<T> {

    void enqueue(T item);

    T dequeue();

    boolean isEmpty();

    int size();
}
