package my.study.algorythms.queue;

import java.util.Iterator;

/**
 * Created by dkuchugurov on 25.12.2014.
 */
public class LinkedQueue<T> implements Queue<T> {

    private Node first;

    private Node last;

    private int N;


    @Override
    public void enqueue(T item) {

        Node oldLast = last;

        last = new Node(item, null);

        if (isEmpty()) {
            first = last;
        } else {
            oldLast.next = last;
        }
        N++;
    }


    @Override
    public T dequeue() {
        T item = first.item;

        first = first.next;

        N--;

        if (isEmpty()) {
            last = null;
        }
        return item;
    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public int size() {
        return N;
    }

    private class Node {
        T item;
        Node next;

        Node(T item, Node next) {
            this.item = item;
            this.next = next;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node current = first;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                T item = current.item;

                current = current.next;

                return item;
            }

            @Override
            public void remove() {

            }
        };
    }
}
