package my.study.algorythms.bag;

import org.testng.annotations.Test;

import static my.study.algorythms.util.Stringify.asString;

public class BagTest {

    @Test
    public void test() {

        Bag<String> q = new LinkedBag<String>();


        String input = "1 2 3 4 5 - 6 - - 7 - - - 8";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            if (!item.equals("-")) {
                q.add(item);
                System.out.println(String.format("enqueued %s, size=%s, contents=%s", item, q.size(), asString(q)));
            }
        }


    }


    @Test
    public void test_remove() {

        Bag<String> q = new LinkedBag<String>();


        String input = "0 1 2 3 4 5 6 7 8 9";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            q.add(item);
            System.out.println(String.format("add %s, contents=%s", item, asString(q)));
        }

        System.out.println(String.format("rem %s, contents=%s", q.remove(5), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.remove(2), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.remove(1), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.remove(0), asString(q)));
    }


    @Test
    public void test_remove_after() {

        Bag<String> q = new LinkedBag<String>();


        String input = "0 1 2 3 4 5 6 7 8 9";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            q.add(item);
            System.out.println(String.format("add %s, contents=%s", item, asString(q)));
        }

        System.out.println(String.format("rem %s, contents=%s", q.removeAfter("5"), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.removeAfter("9"), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.removeAfter("7"), asString(q)));
    }

    @Test
    public void test_insert_after() {

        Bag<String> q = new LinkedBag<String>();


        String input = "0 1 2 3 4 5 6 7 8 9";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            q.add(item);
            System.out.println(String.format("add %s, contents=%s", item, asString(q)));
        }

        System.out.println(String.format("rem %s, contents=%s", q.insertAfter("5", "55"), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.insertAfter("0", "00"), asString(q)));
        System.out.println(String.format("rem %s, contents=%s", q.insertAfter("7", "77"), asString(q)));
    }

    @Test
    public void test_reverse() {

        Bag<String> q = new LinkedBag<String>();


        String input = "0 1 2 3 4 5 6 7 8 9";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            q.add(item);
            System.out.println(String.format("add %s, contents=%s", item, asString(q)));
        }

        q.reverse();
        System.out.println(String.format("reversed, contents=%s", asString(q)));
    }

}