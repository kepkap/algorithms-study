package my.study.algorythms.stack;

import my.study.algorythms.queue.LinkedQueue;
import my.study.algorythms.queue.Queue;
import org.testng.Assert;
import org.testng.annotations.Test;

import static my.study.algorythms.util.Stringify.asString;

public class StackTest {


    @Test
    public void test() throws Exception {
        Stack<String> s = new FixedCapacityStack<String>(100);
//        Stack<String> s = new LinkedStack<String>();

//        String input = "to be or not to - be - - that - - - is";
        String input = "it was - the best - of times - - - it was - the - -";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            if (!item.equals("-")) {
                s.push(item);
                System.out.println(String.format("pushed %s, size=%s, contents=%s", item, s.size(), asString(s)));
//                System.out.println(String.format("peek %s, size=%s, contents=%s", s.peek(), s.size(), asString(s)));
            } else if (!s.isEmpty()) {
                System.out.println(String.format("popped %s, size=%s, contents=%s", s.pop(), s.size(), asString(s)));
//                System.out.println(String.format("peek %s, size=%s, contents=%s", s.peek(), s.size(), asString(s)));
            }
        }
    }

    @Test
    public void test_ex1() throws Exception {
        Stack<String> s = new LinkedStack<String>();

        String input = "0 1 2 3 4 5 6 7 8 9";
        System.out.println(input);

        String[] split = input.split(" ");

        for (int i = 0; i < split.length; i++) {
            String item = split[i];
            if (i % 2 == 0) {
                s.push(item);
                System.out.println(String.format("pushed %s, size=%s, contents=%s", item, s.size(), asString(s)));
            } else {
                System.out.println(String.format("popped %s, size=%s, contents=%s", s.pop(), s.size(), asString(s)));
            }
        }

    }

    @Test
    public void test_concurrent_modification() throws Exception {
        Stack<String> s = new LinkedStack<String>();

        String input = "0 1 2 3 4 5 6 7 8 9";
        System.out.println(input);

        String[] split = input.split(" ");

        for (int i = 0; i < split.length; i++) {
            String item = split[i];
            s.push(item);
        }

        for (String s1 : s) {
            System.out.println(String.format(s1));
//            s.push("asd");
        }

    }

    @Test
    public void test_ex134() throws Exception {
        Stack<String> stack = new LinkedStack<String>();

        String properInput = "[()]{}{[()()]()}";
        String invalidInput = "[(])";
        String input = properInput;

        System.out.println(input);
        char[] chars = input.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char aChar = chars[i];

            switch (aChar) {
                case '[':
                    System.out.println(String.format("push %s, contents=%s", stack.push(String.valueOf(aChar)), asString(stack)));
                    break;
                case '(':
                    System.out.println(String.format("push %s, contents=%s", stack.push(String.valueOf(aChar)), asString(stack)));
                    break;
                case '{':
                    System.out.println(String.format("push %s, contents=%s", stack.push(String.valueOf(aChar)), asString(stack)));
                    break;
                case '}':
                    String pop = stack.pop();
                    String msg = String.format("expected-%s, popped=%s, contents=%s", "{", pop, asString(stack));
                    System.out.println(msg);
                    if (!"{".equals(pop)) {
                        throw new RuntimeException(msg);
                    }
                    break;
                case ')':
                    pop = stack.pop();
                    msg = String.format("expected-%s, popped=%s, contents=%s", "(", pop, asString(stack));
                    System.out.println(msg);
                    if (!"(".equals(pop)) {
                        throw new RuntimeException(msg);
                    }
                    break;
                case ']':
                    pop = stack.pop();
                    msg = String.format("expected-%s, popped=%s, contents=%s", "[", pop, asString(stack));
                    System.out.println(msg);
                    if (!"[".equals(pop)) {
                        throw new RuntimeException(msg);
                    }
                    break;
                default:
                    break;
            }
        }

        Assert.assertEquals(stack.size(), 0);

    }

    @Test
    public void test_ex_135() {
        int N = 50;
        Stack<Integer> stack = new LinkedStack<Integer>();
        while (N > 0) {
            stack.push(N % 2);
            N = N / 2;
        }
        System.out.println(asString(stack));
    }

    @Test
    public void test_ex_136() {
        Stack<String> stack = new LinkedStack<String>();
        Queue<String> q = new LinkedQueue<String>();

        q.enqueue("1");
        q.enqueue("2");
        q.enqueue("3");
        System.out.println(asString(q));

        while (!q.isEmpty()) {
            stack.push(q.dequeue());
        }
        while (!stack.isEmpty()) {
            q.enqueue(stack.pop());
        }

        System.out.println(asString(q));
    }

    @Test
    public void test_ex_139() {
        String input = "1 + 2 ) * 3 - 4 ) * 5 - 6 ) ) )";

        String expected = "( ( 1 + 2 ) * ( ( 3 - 4 ) * ( 5 - 6 ) ) )";

        Stack<String> vals = new LinkedStack<String>();
        Stack<String> ops = new LinkedStack<String>();

        String[] split = input.split(" ");

        for (String s : split) {

            if (s.equals("+") || s.equals("-") || s.equals("*")) {
                ops.push(s);
            } else if (s.equals(")")) {
                String v2 = vals.pop();
                String v1 = vals.pop();

                String exp = "( " + v1 + " " + ops.pop() + " " + v2 + " )";
                vals.push(exp);
            } else {
                vals.push(s);
            }
        }

        System.out.println(input);
        System.out.println(" " + expected);
        System.out.println(asString(vals));
    }


}