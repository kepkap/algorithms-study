package my.study.algorythms.queue;

import org.testng.annotations.Test;

import static my.study.algorythms.util.Stringify.asString;

public class JosephusTest {


    @Test
    public void test() {

        Queue<String> queue = new LinkedQueue<String>();

        int k = 2;

        String input = "0 1 2 3 4 5 6 7";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            queue.enqueue(item);
        }

        execute(queue, k);

    }

    private Queue<String> execute(Queue<String> queue, int k) {
        Queue<String> tmp = new LinkedQueue<String>();
        int N = queue.size();

        if (queue.size() == 1) {
            return queue;
        }


        System.out.println(String.format("starting execution %s", asString(queue)));
        for (int i = 0; i < N; i++) {
            if (i % k != 0) {
                String dequeue = queue.dequeue();
                tmp.enqueue(dequeue);
//                System.out.println(String.format("saving %s, ", dequeue));
            } else {
                String dequeue = queue.dequeue();
                System.out.println(String.format("killing %s", dequeue));
            }
        }

        System.out.println(String.format("saved lives %s", asString(tmp)));

        return execute(tmp, k);
    }

    private void printQueues(Queue<String> queue1, Queue<String> queue2) {
        System.out.println(String.format("queue1= %s", asString(queue1)));
        System.out.println(String.format("queue2= %s", asString(queue2)));
    }

}