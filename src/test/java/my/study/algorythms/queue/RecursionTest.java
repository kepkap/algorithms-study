package my.study.algorythms.queue;

import my.study.algorythms.util.StdOut;
import org.testng.annotations.Test;

public class RecursionTest {


    @Test
    public void test() {
        mystery(6);

    }

    public static void mystery(int n) {
        if (n == 0 || n == 1) return;
        mystery(n - 2);
        StdOut.println(n);
        mystery(n - 1);
    }


}