package my.study.algorythms.queue;

import my.study.algorythms.util.Stringify;
import org.testng.annotations.Test;

public class QueueTest {


    @Test
    public void test() {

        Queue<String> q = new LinkedQueue<String>();


        String input = "1 2 3 4 5 - 6 - - 7 - - - 8";
        System.out.println(input);

        String[] split = input.split(" ");

        for (String item : split) {
            if (!item.equals("-")) {
                q.enqueue(item);
                System.out.println(String.format("enqueued %s, size=%s, contents=%s", item, q.size(), Stringify.asString(q)));
            } else if (!q.isEmpty()) {
                System.out.println(String.format("dequeued %s, size=%s, contents=%s", q.dequeue(), q.size(), Stringify.asString(q)));
            }
        }


    }

}